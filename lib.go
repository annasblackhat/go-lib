package go_lib

import "fmt"

func WhoAmI(name, address string) string {
	return fmt.Sprintf("your name is %s, and you are from %s", name, address)
}